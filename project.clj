(defproject clj-jasper "0.1.0"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [local/chinese-fonts "1.2"]   ; for jasperreport pdf 中文显示
                 [korma "0.4.0"]
                 [lib-noir "0.8.9"]
                 [net.sf.jasperreports/jasperreports "5.5.1"]])
