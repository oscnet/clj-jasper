(ns clj-jasper.print
"报表打印
  所有打印使用统一的web path = /print type #{print pdf ..}  report  ... params
  程序中只要定义
  (defmethod prepare-report :report [report param])做打印前的准备工作即可,
  返回传递给报表的MAP参数"
  (:use compojure.core)
  (:require [crystal-cms.jasper :as jasper]
            [korma.db :as korma]
            [noir.response :as response]
            [noir.util.route :refer [def-restricted-routes]]))

(def ^:private custom-reports-path "crystal_cms/views/reports/")

(defmulti prepare-report
  "准备报表 (prepare-report report param)
  report 报表名
  param  前台传递的参数
  返回给报表的参数,如果报表中有sub report , 使用 :sub-reports [ \"report1\" ] 来自动编译子报表
  如果没有对应的函数定义,直接使用param参数"
  (fn [report param]
    (keyword report)))

(defmethod prepare-report :default [report param]
  (println (str "don't know how to prepare-report " report ", use default param"))
  param)

(defn- jasper-print [type report param]
  (let [param (prepare-report report param)]
    (if-let [sub-reports (:sub-reports param)]
      (doseq [r sub-reports]
        (jasper/get-report (str custom-reports-path r))))  ;auto compile sub reports if it's not compiled to jasper
    (try
      (korma/transaction
       (jasper/export
        type
        (str custom-reports-path report)
        (dissoc param  :sub-reports)))
      (catch Exception e
        (.printStackTrace e)
        (if (= :print (keyword type))
          (response/json {:status -1 :alert (.getMessage e)})
          (.getMessage e))))))

(def-restricted-routes print-routes
  (GET "/print" [type report & param] (jasper-print type report param)))
