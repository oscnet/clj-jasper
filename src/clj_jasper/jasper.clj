(ns clj-jasper.jasper
  (:import (net.sf.jasperreports.engine
            JRExporter
            JasperCompileManager
            JasperFillManager
            JRExporterParameter)
           (net.sf.jasperreports.engine.export
            JRPdfExporter
            HtmlExporter
            JRPrintServiceExporter
            JRPrintServiceExporterParameter)
           (java.util HashMap Map)
           (java.net URL))
  (:require [korma.db :as korma]
            [noir.response :as response]
            [clojure.java.jdbc :as jdbc]
            [ring.util.io :refer [piped-input-stream]]))

(defn- ^{:tag URL} resource-path [path]
  (-> (Thread/currentThread)
      (.getContextClassLoader)
      (.getResource path)))

(defn- compile-report
  "编译 jasper 报表,返回.jasper文件路径"
  [report]
    (->
     (str report ".jrxml")
     (resource-path)
     (.getPath)
     (JasperCompileManager/compileReportToFile)))

(defn ^{:tag String} get-report
  "返回.jasper报表路径, report为报表名,不包括扩展名.
  如果报表未经没有编译,自动编译并返回"
  [report]
  (if-let [f (resource-path (str report ".jasper"))]
    (.getPath f)
    (compile-report report)))

(defn- ^{:tag Map} keyword->name
  "change {:a 'abc'} to {'a' 'abc'}"
  [m]
  (reduce
   (fn [m [ k v ]]
     (assoc m (name k) v))
   {}
   (seq m)))

(defn fill-report
  "fill the report
   (korma.db/transaction
     (fill-report report param))
   (jdbc/with-connection conn
     (fill-report report param))
  "
  [report param]
  {:pre [(jdbc/find-connection)]}
    (JasperFillManager/fillReport
     (-> report (get-report))
     (-> param (keyword->name) (java.util.HashMap.))
     (jdbc/find-connection)))

(defn- exportReport
  "exporter is a JRPdfExporter or HtmlExporter and so on"
  [report ^JRExporter exporter]
  (piped-input-stream
   (fn [output]
     (doto exporter
       (.setParameter JRExporterParameter/JASPER_PRINT report)
       (.setParameter JRExporterParameter/OUTPUT_STREAM output)
       (.exportReport)))))

(defmulti ^{:tag JRExporter} export
  "导出报表
  (export exporter report param)
  exporer is one of  :pdf :html :print ,:print 为直接打印,可以在 param 中设置 :show-dialog? 指定是不是显示打印设置对话框.
  report 报表名, 包括路径,但不包括扩展名.
  param  报表参数
  example: (export :pdf \"order\" {:id 1})"
  (fn [exporter _ _]
    (keyword exporter)))

(defmethod export :pdf [exporter report param]
  (response/content-type
   "application/pdf"
   (exportReport
    (fill-report report param)
    (new JRPdfExporter))))

(defmethod export :html [exporter report param]
  (exportReport
   (fill-report report param)
   (new HtmlExporter)))

(defmethod export :print [exporter report param]
  (let [^JRExporter exporter (JRPrintServiceExporter.)]
    (doto exporter
      (.setParameter JRExporterParameter/JASPER_PRINT (fill-report report param))
      (.setParameter JRPrintServiceExporterParameter/DISPLAY_PAGE_DIALOG Boolean/FALSE)
      (.setParameter JRPrintServiceExporterParameter/DISPLAY_PRINT_DIALOG, (or (:show-dialog? param) false))
      (.exportReport))
      (response/json {:status 0})))

(defmethod export :default [exporter report param]
  (throw (IllegalArgumentException.
          (str "I don't know how to report for " exporter ))))
