# clj-jasper

A Clojure library designed for jasper report

## Usage

 jasper must install chinese fonts
    brew install Maven
    mvn install:install-file -Durl=file:repo -DgroupId=local -DartifactId=chinese-fonts -Dversion=1.2 -Dpackaging=jar -Dfile=chinese-fonts.jar

## License

Copyright © 2014 FIXME

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
